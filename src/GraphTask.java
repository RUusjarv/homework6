import java.util.*;

import javax.management.RuntimeErrorException;

//Koostada meetod etteantud graafi transitiivse sulundi leidmiseks.
//http://enos.itcollege.ee/~jpoial/algoritmid/graafid.html
//Matrixiks tegemiseks sain kuskilt internetist natuke abi aga kahjuks ei leia linki �les

public class GraphTask {

	public static void main(String[] args) {
		GraphTask a = new GraphTask();
		a.run1();
		a.run2();
		a.run3();
		a.run4();
		a.run5();
	}

	public void run1() {
		System.out.println("Algne graaf, kus on vaid �ks v�imalik tee");
		Graph g = new Graph("Esimene test");
		g.createRandomSimpleGraph(2, 1);
		System.out.println(g);
		g.transitiveClosure();

	}
	public void run2() {
		System.out.println("Algne graaf, kus on vait tipp");
		Graph g = new Graph("Teine test");
		g.createRandomSimpleGraph(1, 0);
		System.out.println(g);
		g.transitiveClosure();

	}
	public void run3() {
		System.out.println("Algne graaf, mis lahendub ja on v�ike");
		Graph g = new Graph("Kolmas test");
		g.createRandomSimpleGraph(4, 5);
		System.out.println(g);
		g.transitiveClosure();


	}
	public void run4() {
		System.out.println("Algne graaf, mis lahendub ja on suur");
		Graph g = new Graph("Neljas test");
		g.createRandomSimpleGraph(20, 20);
		System.out.println(g);
		g.transitiveClosure();

	}
	public void run5() {
		System.out.println("Algne graaf, kus pole tippe ega v�imalikke teid");
		Graph g = new Graph("Viies test");
		g.createRandomSimpleGraph(0, 0);
		System.out.println(g);
		g.transitiveClosure();

	}

	class Vertex {

		private String id;
		private Vertex next;
		private Arc first;
		private int info = 0;

		Vertex(String s, Vertex v, Arc e) {
			id = s;
			next = v;
			first = e;
		}

		Vertex(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}
		// TODO!!! Your Vertex methods here!
	}

	class Arc {

		private String id;
		private Vertex target;
		private Arc next;

		Arc(String s, Vertex v, Arc a) {
			id = s;
			target = v;
			next = a;
		}

		Arc(String s) {
			this(s, null, null);
		}

		@Override
		public String toString() {
			return id;
		}

		// TODO!!! Your Arc methods here!
	}

	/**
	 * @author Riho
	 *
	 */
	class Graph {

		private String id;
		private Vertex first;
		private int info = 0;

		Graph(String s, Vertex v) {
			id = s;
			first = v;
		}

		Graph(String s) {
			this(s, null);
		}

		@Override
		public String toString() {
			String nl = System.getProperty("line.separator");
			StringBuffer sb = new StringBuffer(nl);
			sb.append(id);
			sb.append(nl);
			Vertex v = first;
			while (v != null) {
				sb.append(v.toString());
				sb.append(" -->");
				Arc a = v.first;
				while (a != null) {
					sb.append(" ");
					sb.append(a.toString());
					sb.append(" (");
					sb.append(v.toString());
					sb.append("->");
					sb.append(a.target.toString());
					sb.append(")");
					a = a.next;
				}
				sb.append(nl);
				v = v.next;
			}
			return sb.toString();
		}

		public Vertex createVertex(String vid) {
			Vertex res = new Vertex(vid);
			res.next = first;
			first = res;
			return res;
		}

		public Arc createArc(String aid, Vertex from, Vertex to) {
			Arc res = new Arc(aid);
			res.next = from.first;
			from.first = res;
			res.target = to;
			return res;
		}

		/**
		 * Create a connected undirected random tree with n vertices. Each new
		 * vertex is connected to some random existing vertex.
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */
		public void createRandomTree(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
				if (i > 0) {
					int vnr = (int) (Math.random() * i);
					createArc("a" + varray[vnr].toString() + "_" + varray[i].toString(), varray[vnr], varray[i]);
					createArc("a" + varray[i].toString() + "_" + varray[vnr].toString(), varray[i], varray[vnr]);
				} else {
				}
			}
		}

		/**
		 * Teeb t�hja puu, mille t�idan matrixi abil
		 * 
		 * @param n
		 *            number of vertices added to this graph
		 */

		public void createRandomEmthyTree(int n) {
			if (n <= 0)
				return;
			Vertex[] varray = new Vertex[n];
			for (int i = 0; i < n; i++) {
				varray[i] = createVertex("v" + String.valueOf(n - i));
			}
		}

		/**
		 * Create an adjacency matrix of this graph. Side effect: corrupts info
		 * fields in the graph
		 * 
		 * @return adjacency matrix
		 */
		public int[][] createAdjMatrix() {
			info = 0;
			Vertex v = first;
			while (v != null) {
				v.info = info++;
				v = v.next;
			}
			int[][] res = new int[info][info];
			v = first;
			while (v != null) {
				int i = v.info;
				Arc a = v.first;
				while (a != null) {
					int j = a.target.info;
					res[i][j]++;
					a = a.next;
				}
				v = v.next;
			}
			return res;
		}

		/**
		 * Create a connected simple (undirected, no loops, no multiple arcs)
		 * random graph with n vertices and m edges.
		 * 
		 * @param n
		 *            number of vertices
		 * @param m
		 *            number of edges
		 */
		public void createRandomSimpleGraph(int n, int m) {
			if (n <= 0)
				return;
			if (n > 2500)
				throw new IllegalArgumentException("Too many vertices: " + n);
			if (m < n - 1 || m > n * (n - 1) / 2)
				throw new IllegalArgumentException("Impossible number of edges: " + m);
			first = null;
			createRandomTree(n); // n-1 edges created here
			Vertex[] vert = new Vertex[n];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				vert[c++] = v;
				v = v.next;
			}
			int[][] connected = createAdjMatrix();
			int edgeCount = m - n + 1; // remaining edges
			while (edgeCount > 0) {
				int i = (int) (Math.random() * n); // random source
				int j = (int) (Math.random() * n); // random target
				if (i == j)
					continue; // no loops
				if (connected[i][j] != 0 || connected[j][i] != 0)
					continue; // no multiple edges
				Vertex vi = vert[i];
				Vertex vj = vert[j];
				createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
				connected[i][j] = 1;
				createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
				connected[j][i] = 1;
				edgeCount--; // a new edge happily created
			}
		}

		/**
		 * Vaja matrixi tegemisel, abi sain lehelt stackoverflow, kahjuks j�in link kirja panemata
		 * 
		 * @param str
		 *            string graafist
		 * @return vertexite kogus
		 */
		private int countVertex(String str) {
			String findStr = "-->";
			int lastIndex = 0;
			int count = 0;

			while (lastIndex != -1) {
				lastIndex = str.indexOf(findStr, lastIndex);
				if (lastIndex != -1) {
					count++;
					lastIndex += findStr.length();
				}
			}
			return count;
		}

		/**
		 * Vaja matrixi tegemisel, abi sain lehelt stackoverflow, kahjuks j�in link kirja panemata
		 * 
		 * @param str
		 *            string graafist
		 * @param int
		 *            vertexite arc
		 * @return stringide arrray arcidega
		 */

		private String[] separateLines(String str, int n) {
			String textStr[] = str.split("\\r?\\n");
			String separateLines[] = new String[n];
			int counter = 0;

			for (int i = 0; i < textStr.length; i++) {
				int l = textStr[i].indexOf(">");
				if (l != -1) {
					separateLines[counter] = textStr[i];
					counter++;
				}
			}
			return separateLines;
		}

		/**
		 * Vaja matrixi tegemisel, abi sain lehelt stackoverflow, kahjuks j�in link kirja panemata
		 * 
		 * @param int
		 *            vertexite arc
		 * @return stringide arrray vertexidest
		 */

		private String[] vertexStringArray(int n) {
			String vertexes[] = new String[n];
			for (int i = 0; i < n; i++) {
				int m = i + 1;
				vertexes[i] = "v" + m;
			}
			return vertexes;
		}

		/**
		 * teeb stringidest kokku matrixi, abi sain lehelt stackoverflow, kahjuks j�in link kirja panemata
		 * 
		 * @return matrix
		 */

		public int[][] toMatrix() {
			String str = this.toString();
			int n = countVertex(str);
			String singleLines[] = separateLines(str, n);
			String vertexes[] = vertexStringArray(n);
			int matrix[][] = new int[n][n];

			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (singleLines[i].contains(vertexes[j]) && i != j) {
						matrix[i][j] = 1;
					} else {
						matrix[i][j] = 0;
					}
				}
			}
			return matrix;
		}

		/**
		 * Matrixi printimiseks
		 */

		public void printArray(int matrix[][]) {
			for (int row = 0; row < matrix.length; row++) {
				for (int column = 0; column < matrix[row].length; column++) {
					System.out.print(matrix[row][column] + " ");
				}
				System.out.println();
			}
		}

		/**
		 * V�ljastab transitive Closure. Tehes selle algselt matrixiks,
		 * lahendades selle ja lahendatud matrixist graph tagasi
		 * 
		 * @return v�lja prinditud matrix
		 */
		public void transitiveClosure() {
			// lahendab Matrixi

			int[][] graaf = this.toMatrix();
			int pikkus = graaf.length;
			if (pikkus == 0){
				throw new RuntimeException("Graaf on t�hi");
			}
			System.out.println("Algne matrix");
			printArray(graaf);
			System.out.println();
			System.out.println("Valmis matrix");
			for (int i = 0; i < pikkus; i++) {
				for (int j = 0; j < pikkus; j++) {
					// l�bin matrixi
					if (graaf[i][j] == 1) {
						// leian koha, kus on paigutatud 1 ehk saab edasi minna
						for (int s = 0; s < pikkus; s++) {
							// l�hen sellele reale ja otsin sealt �he, et saaks
							// algsele reale lisada ja asukoht ei tohi kattuda
							// diagonaaliga
							if (graaf[j][s] == 1 && i != s && graaf[i][s] == 0) {
								graaf[i][s] = 1;
								i=0;
								j=0;
								s=0;
							}
						}
					}
				}
			}
			printArray(graaf);
			System.out.println();
			Graph s = new Graph("G vastus");
			s.createGraph(graaf, pikkus);
			System.out.println(s);
		}

		/**
		 * Teeb matrixist graafi, samalaadselt kui �ppej�u graafi meetod
		 * 
		 * @param int graaf - marix millest graaf tehakse
		 * @param int pikkus - palju vertexe on graafi tegemiseks
		 * @return stringide arrray vertexidest
		 */
		public void createGraph(int graaf[][], int pikkus) {
			System.out.println("Valmis graaf");

			first = null;
			createRandomEmthyTree(pikkus);
			Vertex[] uus = new Vertex[pikkus];
			Vertex v = first;
			int c = 0;
			while (v != null) {
				uus[c++] = v;
				v = v.next;
			}
			int[][] connected = createAdjMatrix();
			for (int i = 0; i < pikkus; i++) {
				for (int j = 0; j < pikkus; j++) {
					if (graaf[i][j] == 1) {
						Vertex vi = uus[i];
						Vertex vj = uus[j];
						createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
						connected[i][j] = 1;

					}
				}
			}

		}

	}

}
